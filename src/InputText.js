import * as React from 'react';

export class InputText extends React.Component {
    onChange = (e) => {
        const {onChange, name} = this.props;
        const {value} = e.target;
        if (onChange) {
            onChange(name, value);
        }
    }

    render() {
        const {name, title, type, placeholder, value} = this.props;
        return (
            <div className="form-group">
                <label htmlFor={name}>{title}</label>
                    <input
                        className={'form-control'}
                        placeholder={placeholder}
                        name={name}
                        id={name}
                        type={type}
                        value={value}
                        onChange={this.onChange}
                        required
                    />
            </div>
        );
    }
}