import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Flexi } from './Flexi.js';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';


class App extends Component {
    onFlexiSubmit = (name, state) => {
        document.getElementById('userInput').innerHTML = 'Mr. ' + name + ' belongs to ' + state;
    }
  render() {
      const flexiConfig = {
          items: [
              {
                  "name": "personname",
                  "label": "Person's Name",
                  "type": "TextField",
                  "placeholder": "Enter first name"
              },
              {
                  "name": "states",
                  "label": "Person's state",
                  "type": "DropDown",
                  "placeholder": "select your state",
                  "defaultValue": "Maharashtra",
                  "values": [
                      "Maharashtra",
                      "Kerala",
                      "Tamil Nadu"
                  ]
              }
          ]
      };
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
          <div className="container">
              <div className="jumbotron">
                <Flexi onSubmit={this.onFlexiSubmit} config={flexiConfig}/>
              </div>
              <div id="userInput" className="card card-header">
              </div>
          </div>
      </div>
    );
  }
}

export default App;
