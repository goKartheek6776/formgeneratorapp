import * as React from 'react';
import { InputText } from './InputText';
import { Dropdown } from './Dropdown';

export class Flexi extends React.Component {

    constructor(props) {
        super(props);
        var defaultValues = {};
        this.props.config.items.forEach((ele) => {
            if (ele.defaultValue) {
               defaultValues[ele.name] = ele.defaultValue;
            }
        });
        this.state = defaultValues;
    }

    onChange = (name, value) => {
        this.setState({[name]: value});
    }

    submit = (event) => {
        this.props.onSubmit(this.state.personname, this.state.states);
        event.preventDefault();
    }

    render() {
        const {title, type, placeholder, value} = this.props;
        const list = this.props.config.items.map(input => {
            if (input.type === "TextField") {
                return (
                    <InputText
                        value={this.state[input.name]}
                        key={input.name}
                        type={input.type}
                        name={input.name}
                        title={input.label}
                        className={input.class}
                        placeholder={input.placeholder}
                        onChange={this.onChange}
                    />
                );
            } else if (input.type === "DropDown") {
                return (
                    <Dropdown
                        value={this.state[input.name]}
                        key={input.name}
                        type={input.type}
                        name={input.name}
                        title={input.label}
                        className={input.class}
                        placeholder={input.placeholder}
                        options={input.values}
                        onChange={this.onChange}
                    />
                );
            }

        });

        return (
            <form role="form" onSubmit={this.submit}>
                 {list}
                <button className="btn btn-info spacing">Submit</button>
            </form>
        );
    }
}