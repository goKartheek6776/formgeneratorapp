import * as React from 'react';

export class Dropdown extends React.Component {
    onChange = (e) => {
        const {onChange, name} = this.props;
        const {value} = e.target;
        if (onChange) {
            onChange(name, value);
        }
    }

    render() {
        const {name, title, type, placeholder, value, options} = this.props;
        return (
            <div className="form-group">
                <label htmlFor={name}>{title}</label>
                <select
                    className={'form-control'}
                    placeholder={placeholder}
                    name={name}
                    id={name}
                    type={type}
                    value={value}
                    onChange={this.onChange}>
                    required
                    {options.map(function(data, i){
                        return <option value={data} key={i}>{data}</option>;
                    })}
                </select>
            </div>
        );
    }
}